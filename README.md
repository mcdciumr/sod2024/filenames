# Namenskonventionen prüfen

Pipelines können genutzt werden, um automatisch auf die Einhaltung von Konventionen bei
Dateinamen und Code zu prüfen.

- Testfall: Wir prüfen Dateinamen auf korrekte Formatierung und Groß- und Kleinschreibung.
- Warum? Weil Multiplattform-Projekte zu Bugs führen können, wenn z.B. bei Groß- und Kleinschreibung nicht sauber
  gearbeitet wird. Bsp: Datei `image.PNG` wird in Code oder autogenerierten Dateien als `image.png` referenziert. Führt
  nicht zu Problemen auf Windows, aber auf Linux-Systemen.
- Für den Test verwenden wir [reguläre Ausdrücke](https://de.wikipedia.org/wiki/Regul%C3%A4rer_Ausdruck), um die Dateien
  zu erkennen, die unsere Regeln verletzen.

```yaml
image: ubuntu:22.04

test:
  script:
    - rm -r README.md .gitlab-ci.yml .git
    - RESULT=$(find ./ -type f -regex '.*[A-Z -]+.*' | wc -l)
    - if [ $RESULT == 0 ]; then exit 0; fi
    - echo "The following filenames don't match the project's filename guidelines:"
    - find ./ -type f -regex '.*[A-Z -]+.*'
    - exit 1
  rules:
   - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
```

- Der Erfolg von Pipelines wird durch den Wert angegeben, der nach Abschluss des Skripts zurückgegeben wird.
  Standard ist `exit 0` (muss nicht explizit ausgeschrieben werden) und bedeutet, dass das Skript ohne Fehler
  beendet wurde. Jede andere Zahl (Konvention: `exit 1`) bedeutet, dass das Skript durch einen Fehler abbricht.
- Eine Pipeline lädt das Git-Repository in das Docker-Image und führt alle Befehle im Repo-Ordner aus. 
- Dateien können beliebig gelöscht (`rm -r README.md .gitlab-ci.yml .git`), erzeugt und umbenannt werden.
- **Rules** bestimmen, unter welchen Bedingungen die Pipeline ausgeführt wird.
